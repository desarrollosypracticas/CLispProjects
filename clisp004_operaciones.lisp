;Operaciones

(setq *print-case* :capitalize)


(format t "(expt 4 2) = ~d ~%" (expt 4 2))

(format t "(sqrt 81) = ~d ~%" (sqrt 81))

(format t "(exp 1) = ~d ~%" (exp 1))

(format t "(log 1000 10) = ~d ~%" (log 1000 10)) ; =3 = Because 10^3 = 1000

(format t "(eq 'dog 'dog) = ~d ~%" (eq 'dog 'dog))

(format t "(floor 5.5) = ~d ~%" (floor 5.5))

(format t "(ceiling 5.5) = ~d ~%" (ceiling 5.5))

(format t "(max 5 10)  = ~d ~%" (max 5 10))

