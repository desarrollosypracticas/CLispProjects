Ejercicios de clisp.


![alt tag](https://codeyarns.files.wordpress.com/2011/11/20111126-clisp.png)

## Lista de Ejercicios Assembler en CLisp
|          | Nombre del Ejercicio          | Descripcion   |
|----------|-------------------------------|---------------|
| clisp001 | hola                          | Pide nombre e imprime saludo.
| clisp002 | formatonumeros                | Imprime diferentes maneras derepresentar numeros en clisp.
| clisp003 | operacionesbasicas            | Calcula operaciones básicas.
| clisp004 | operaciones                   | Calcula operaciones a base de funciones matemáticas.
| clisp005 | hello                         | Imprime texto en pantalla.
| clisp006 | loop1                         | Hace un loop básico imprimiento.
| clisp007 | loop2                         | Hace un loop uniendo dos listas.
| clisp008 | loop3                         | Hace un loop añadiendo elemendos a lista separados por coma.
| clisp009 | sumcuadrados                  | Define una funcion en LISP para calcular la suma de 2 cuadrados.
| clisp010 | tableloop                     | Define diferentes funciones utilizando el loop.
