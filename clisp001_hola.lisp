;Comment

;(format t "Hello world ~%")

(print "What's your name?")

(defvar *name* (read))

(defun hello-you (name)
    (format t "Hello ~a! ~%" name)
)

; ~a: Shows the value
; ~s: Shows quotes around the value
; ~10a: Adds 10 spaces for the value with extra space t
; ~10@a: Adds 10 spaces fot the value with extra space

(setq *print-case* :capitalize)

(hello-you *name*)