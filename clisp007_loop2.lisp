;Iterate through two lists in parallel, and cons up a result that returned as a 
;value by loop

(loop for x in '(a b c d e)
      for y in '(1 2 3 4 5)
      collect (list x y)
)

