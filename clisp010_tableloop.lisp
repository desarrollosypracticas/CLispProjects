
;Simple loop
(loop 
	(princ
		"type something"
	(force-output)
	(read)
	)
)


;do*
(loop for i below 5
	do (princ i))

;repeat
(loop repeat 5
	do (print
	"Prints five times"))

;return
(loop for i below 10
	when (= i 5)
	return
		'leaving-early
	do (print i))

;initially
(loop initially
	(print
	'loop-begin)
	for x below 3
	do (print x))

;finally
(loop for x below 3
	do (print x)
	finally
	(print 'loop-end))

;named
(loop named outer)
