;Define una funcion en LISP para calcular la suma de 2 cuadrados.

(defun scuadrados(x y)
	(+ (* x x)(* y y))

);End defun
