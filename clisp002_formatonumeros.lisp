;Clisp 002: Representacion de numeros

(setq *print-case* :capitalize)

(format t "Number with commas ~:d ~%" 10000000)

(format t "PI to 5 characters ~5f ~%" 3.141593)

(format t "PI to 4 decimals ~,4f ~%" 3.141593)

(format t "10 percent ~,,2f ~%" .10)

(format t "10 Dollars ~S ~%" 10)

;~% es un salto de linea
;format t imprime con determinado formato